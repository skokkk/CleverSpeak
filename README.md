# CleverSpeak

CleverSpeak is a TeamSpeak ServerQuery bot powered by [PlanetTeamspeak] and [CleverBot]. 

## Note:

It is always a pleasure seeing others fork this project and use the ideas I give to the community. I love the fact that SinusBot has added CleverSpeak, although re-written, to their default installed plugins. It is in no way the same code directly, the written way is maybe the same, but it is amazing to see that a small project becomes part of a huge commercial TeamSpeak paid-for system!

### Version
1.0

### Installation

Get a [CleverBot Key] and insert the required values:

```sh
// bot name & query login name & cleverbot api instance name
$name = "cleverbot";
// query password
$pw = "mySecur3p@ssw0rd";
// server address
$addr = "voice.teamspeak.com";
// server query port
$qport = 10011;
// cleverbot api username (https://cleverbot.io/keys)
$cb_user = "XX";
// cleverbot api key (https://cleverbot.io/keys)
$cb_key = "XXX";
```

### Development
Do whatever the fuck you want and name it whatever the fuck you want :).


License
----

[WTFPL]

   [PlanetTeamSpeak]: <https://docs.planetteamspeak.com/ts3/php/framework/index.html>
   [CleverBot]: <https://cleverbot.io/>
   [CleverBot Key]: <https://cleverbot.io/keys>
   [WTFPL]: <www.wtfpl.net/>