<?php

// bot name & query login name & cleverbot api instance name
$name = "cleverbot";
// query password
$pw = "mySecur3p@ssw0rd";
// server address
$addr = "voice.teamspeak.com";
// server query port
$qport = 10011;
// cleverbot api username (https://cleverbot.io/keys)
$cb_user = "XX";
// cleverbot api key (https://cleverbot.io/keys)
$cb_key = "XXX";

// some advanced stuff - do not touch
$url = 'https://cleverbot.io/1.0/ask';

// load framework files
require_once("libraries/TeamSpeak3/TeamSpeak3.php");

// connect to local server in non-blocking mode, authenticate and spawn an object for the virtual server on port 9987
$ts3_VirtualServer = TeamSpeak3::factory("serverquery://".$name.":".$pw."@".$addr.":".$qport."/?server_port=9987&blocking=0&nickname=".$name);

// get notified on incoming private messages
$ts3_VirtualServer->notifyRegister("textprivate");

// register a callback for notifyTextmessage events 
TeamSpeak3_Helper_Signal::getInstance()->subscribe("notifyTextmessage", "onTextmessage");

// wait for events
while(1) $ts3_VirtualServer->getAdapter()->wait();

// define a callback function
function onTextmessage(TeamSpeak3_Adapter_ServerQuery_Event $event, TeamSpeak3_Node_Host $host)
{
	global $cb_user;
	global $cb_key;
	global $url;
	global $name;
	global $ts3_VirtualServer;

	// receive message
	echo "Client " . $event["invokername"] . " sent textmessage: " . $event["msg"] . "\n";
	$cb_msg = $event["msg"];
	$reply_user = $event["invokername"];

	if ($reply_user != $name)
	{
		// query cleverbot
		$data = array('user' => $cb_user, 'key' => $cb_key, 'nick' => $name, 'text' => $cb_msg);
		$options = array(
		    'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		    )
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		$ts3_Client = $ts3_VirtualServer->clientGetByName($reply_user);
		if ($result === FALSE) { 
				$ts3_Client->message("I'm a bit confused. Please rephrase that."); 
			} else {
				$reply = json_decode($result, true);
				// reply to user
			 	$ts3_Client->message($reply["response"]);
			}

	}
}
?>
